<?php
/**
 * @file admiral_landing_pages.module
 */

/**
 * Implements hook_init().
 */
function admiral_landing_pages_init() {

    //Get current path
    $path = $_GET['q'];
    $path_alias = drupal_get_path_alias($path);

    //Check for cookie
    if (isset($_COOKIE['admiral_charity_cards_charity'])) {
        $charity = $_COOKIE['admiral_charity_cards_charity'];
        if ($charity != $path_alias  && drupal_is_front_page()) {
            drupal_goto($charity);
        }
    }

    //Narrow down the pages to test if it is a landing page
    if(strpos('/', $path_alias) == false) {
        //Collect the landing pages
        $landing_pages = variable_get('admiral_landing_pages', '');
        //Check if cookie has already been set. If not set it.
        if (!isset($_COOKIE['admiral_charity_cards_charity'])) {
            //Test if the current page is one of our landing_pages
            if (strpos($landing_pages, $path_alias)) {
                //Set the cookie
                global $cookie_domain;
                setrawcookie('admiral.charity.cards.charity', rawurlencode($path_alias), REQUEST_TIME + 10368000, '/', $cookie_domain);
            }
        }
    }
}

/**
 *Implements theme_preprocess_page
 */
function admiral_landing_pages_preprocess_page(&$variables) {
    //Get the landing pages
    $pages = variable_get('admiral_landing_pages');
    $pages = explode(',', $pages);
    $url = current_path();

    if (in_array($url, $pages)) {
        $variables['classes_array'][] = 'landing_page';
        //Load the taxonomy term to determine page title
        $term = taxonomy_get_term_by_name($url, 'landingpages');
        $term = taxonomy_term_load(key($term));
        if (isset($term->field_page_title[LANGUAGE_NONE][0]['safe_value'])) {
            drupal_set_title($term->field_page_title[LANGUAGE_NONE][0]['safe_value']);
            //Hide the breadcrumb and page title on the page

        }
        $variables['show_title'] = FALSE;
        $variables['show_breadcrumb'] = FALSE;
        //Add the page title to the content_first region
        if (isset($term->field_page_title[LANGUAGE_NONE][0]['safe_value'])) {
            $title = $term->field_page_title[LANGUAGE_NONE][0]['safe_value'];
        }
        else {
            $title = $title = '';
        }

        if($title != '') {
            $page_title = '<div class="cf-page-title"><h1>' .  $title . '</h1></div>';

        }
        else {
            $charity = taxonomy_term_load($term->admiral_charity_select[LANGUAGE_NONE][0]['tid']);
            $page_title = '<div class="cf-page-title"><h1>' .  $charity->name . ' Personalised Christmas Cards</h1></div>';
        }

        $variables['page']['content_first']['more_markup']['#markup'] = $page_title . $variables['page']['content_first']['more_markup']['#markup'];

    }
    else {
        $variables['show_title'] = TRUE;
        $variables['show_breadcrumb'] = TRUE;
    }
}

/**
 * Implements hook_permission().
 */
function admiral_landing_pages_permission() {
    return array(
        'administer admiral landing pages' => array(
            'title' => t('Administer Admiral landing pages'),
            'description' => t('Administer landing pages for charities.'),
        ),
    );
}


/**
 * Implements hook_menu().
 */
function admiral_landing_pages_menu() {

    $items = array();

    $items['admin/config/landing_pages'] = array(
        'title' => 'Admiral Landing Pages',
        'description' => 'Administration of landing pages',
        'position' => 'left',
        'weight' => -100,
        'page callback' => 'system_admin_menu_block_page',
        'access arguments' => array('administer site configuration'),
        'file' => 'system.admin.inc',
        'file path' => drupal_get_path('module', 'system'),
    );

    $items['admin/config/landing_pages/landingpages'] = array(
        'title' => 'Admiral Landing Pages Add',
        'description' => 'Allows a client to add landing pages.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('admiral_landing_pages_form'),
        'access arguments' => array('administer admiral landing pages'),
        'type' => MENU_NORMAL_ITEM,
        'file' => 'admiral_landing_pages.admin.inc',
    );

    $items['admin/config/landing_pages/landingpages/%'] = array(
        'title' => 'Admiral Landing Pages',
        'description' => 'Allows a client to edit landing pages.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('admiral_landing_pages_form',4),
        'access arguments' => array('administer admiral landing pages'),
        'type' => MENU_NORMAL_ITEM,
        'file' => 'admiral_landing_pages.admin.inc',
    );

    $items['admin/config/landing_pages/deletelandingpages'] = array(
        'title' => 'Admiral Landing Pages Manage',
        'description' => 'Allows a client to edit and delete landing pages.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('admiral_landing_pages_delete_form'),
        'access arguments' => array('administer admiral landing pages'),
        'type' => MENU_NORMAL_ITEM,
        'file' => 'admiral_landing_pages.admin.inc',
    );

    //Dynamically created menu items for Landing pages
    $urls = variable_get('admiral_landing_pages', '');
    if ($urls != '') {
        $existing_urls = explode(',', $urls);
        foreach($existing_urls as $url) {
            if ($url != '') {
                //Load the related charity term to produce a title
                $lp_term = taxonomy_get_term_by_name($url, 'landingpages');
                $lp_term = taxonomy_term_load(key($lp_term));
                $charity = taxonomy_term_load($lp_term->admiral_charity_select[LANGUAGE_NONE][0]['tid']);

                $items[$url] = array(
                    'title' => $charity->name . ' personalised christmas cards',
                    'page callback' => '_landing_page_content',
                    'page arguments' => array($url),
                    'access callback' => TRUE,
                );
            }
        }
    }

    return $items;
}


/**
 * Dynamic Landing page callback
 */
function _landing_page_content($url = NULL) {

    //If we are on a landing page, the slideshow must be inserted using a contextual filter
    //using the tid of the landingpage term
    $terms = taxonomy_get_term_by_name($url, 'landingpages');
    //There is only one term returned but its buried in an array
    foreach($terms as $term) {
        $tid = $term->tid;
    }
    //Load the view using the term id
    $slider =  views_embed_view('slider_landing_page', 'block_landing_page_slider', $tid);

    //Load the charity content
    $landing_content = views_embed_view('landing_page_content', 'landing_page_content_block', $tid);

    //Load the category listing
    $categories_list = views_embed_view('list_categories', 'block_landing_page');

    //@todo Hack for HCPT
    //Load hcpt specific content
    if ($url == 'hcpt') {
        $categories_list = views_embed_view('hcpt_landing_products', 'block_hcpt_landing_page') . $categories_list;
    }

    $page = array(
        '#theme' => 'page',
        '#type' => 'page',
        'content' => array(
            'system_main' => array(
                'main' => array(
                    '#markup' => $categories_list,
                ),
            ),
            '#sorted' => TRUE,
        ),
        'slideshow' => array(
            'some_markup' => array(
                '#markup' => $slider,
            ),
            '#sorted' => TRUE,
        ),
        'content_first' => array(
            'more_markup' => array(
                '#markup' => $landing_content,
            ),
            '#sorted' => TRUE,
        ),
    );
    return $page;
}