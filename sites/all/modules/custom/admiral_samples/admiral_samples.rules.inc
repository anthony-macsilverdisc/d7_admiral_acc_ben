<?php

//This hook provides for creating a new rules action.

/**
 * Implementation of hook_rules_action_info()
 */
/*function admiral_samples_rules_action_info() {

  $actions = array(
    'admiral_sample_product' => array( //Callback function name
      'label' => t('Set pricing for samples'),
      'admiral breakppoint' => t('Specify a breakpoint for example 1 card'),
      'parameter' => array(
        'commerce_line_item' => array(
          'type' => 'commerce_line_item',
          'label' => t('Line item')
        ),
        'amount' => array(
          'type' => 'decimal',
          'label' => t('Amount'),
          'description' => t('Specify a breakpoint for example 1 card'),
        ),
        'component_name' => array(
          'type' => 'text',
          'label' => t('Price component type'),
          'description' => t('Price components track changes to prices made during the price calculation process, and they are carried over from the unit price to the total price of a line item. When an order total is calculated, it combines all the components of every line item on the order. When the unit price is altered by this action, the selected type of price component will be added to its data array and reflected in the order total display when it is formatted with components showing. Defaults to base price, which displays as the order Subtotal.'),
          'options list' => 'commerce_line_item_price_component_options_list',
          'default value' => 'base_price',
        ),
        'round_mode' => array(
          'type' => 'integer',
          'label' => t('Price rounding mode'),
          'description' => t('Round the resulting price amount after performing this operation.'),
          'options list' => 'commerce_round_mode_options_list',
          'default value' => COMMERCE_ROUND_HALF_UP,
        ),
      ),
      'group' => t('Commerce Line Item'),
    ),
  );
  return $actions;
}*/

//This is the action described in admiral_samples_rules_action_info()
//This rule will reduce the price to zero for sample items
//This rule will reduce the quantity back to one if the client attempts to adjust
//the quantity in the cart.

/**
 * Rules action: create sample price.
 */
/*function admiral_sample_product($line_item, $amount, $component_name, $round_mode) {
  if (is_numeric($amount)) {
    //wrap the line_item - makes access easier
    $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    //Get the current quantity
    $quantity = $line_item->quantity;
    //Get the unit price
    $unit_price = commerce_price_wrapper_value($wrapper, 'commerce_unit_price', TRUE);

    //The samples are free of charge
    //Set the price to zero

    if (intval($quantity) == intval($amount)) {

      $difference = array(
        'amount' => 0 - $unit_price['amount'],
        'currency_code' => $unit_price['currency_code'],
        'data' => array(),
      );

      // Set the amount of the unit price and add the difference as a component.
      $wrapper->commerce_unit_price->amount = 0;

      $wrapper->commerce_unit_price->data = commerce_price_component_add(
        $wrapper->commerce_unit_price->value(),
        $component_name,
        $difference,
        TRUE
      );
    }
  }
}*/