/**
 * This functions replaces the radios with images on the product display
 * Specifically it is used on the Layout Selection
 */
(function ($) {
  Drupal.behaviors.ajw_product_attributes = {
    attach: function (context, settings) {
      $('.form-type-ajw-product-attributes').addClass('form-type-ajw-product-attributes-ajax');
      $('.form-type-ajw-product-attributes input[type=radio]').hide();
      $('.form-type-ajw-product-attributes label.option').hide();

      $('.form-type-ajw-product-attributes-ajax .description').click(function() {
        var parent = $(this).parent();
        $('input[type=radio]', parent).click();
        $('input[type=radio]', parent).change();
      });
    }
  };
}) (jQuery);
