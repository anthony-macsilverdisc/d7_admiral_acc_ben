<?php
$rangeid=60;
$prevcid=126;
$prevwidth=339;
$interval=false;
$range=array (
  32 => 
  array (
    0 => 211,
    1 => 240,
    2 => 305,
    3 => 601,
    4 => 406,
    5 => 713,
    6 => 458,
    7 => 185,
    8 => 315,
    9 => 313,
    10 => 392,
    11 => 448,
    12 => 215,
    13 => 464,
    14 => 214,
    15 => 568,
    16 => 673,
    17 => 310,
    18 => 544,
    19 => 515,
    20 => 546,
    21 => 504,
    22 => 611,
    23 => 494,
    24 => 576,
    25 => 611,
  ),
  58 => 
  array (
    0 => 219,
    1 => 219,
    'interval' => true,
  ),
  60 => 
  array (
    0 => 368,
    1 => 502,
    2 => 368,
    3 => 496,
    4 => 621,
    5 => 679,
    6 => 633,
    7 => 603,
    8 => 663,
    9 => 533,
    10 => 582,
    11 => 649,
    12 => 647,
    13 => 285,
    14 => 417,
    15 => 614,
    16 => 551,
    17 => 876,
    18 => 617,
    19 => 715,
    20 => 609,
    21 => 715,
    22 => 638,
    23 => 540,
    24 => 492,
    25 => 620,
    26 => 574,
    27 => 777,
    28 => 560,
    29 => 617,
    30 => 566,
    31 => 282,
    32 => 568,
    33 => 282,
    34 => 276,
    35 => 546,
    36 => 171,
    37 => 437,
    38 => 415,
    39 => 393,
    40 => 445,
    41 => 397,
    42 => 280,
    43 => 415,
    44 => 431,
    45 => 227,
    46 => 210,
    47 => 424,
    48 => 229,
    49 => 596,
    50 => 462,
    51 => 418,
    52 => 444,
    53 => 415,
    54 => 335,
    55 => 294,
    56 => 262,
    57 => 451,
    58 => 391,
    59 => 546,
    60 => 427,
    61 => 431,
    62 => 376,
    63 => 303,
    64 => 245,
    65 => 304,
    66 => 339,
  ),
);
?>